## HADOUKEN INFO

Just decided to place somewhere all my modifications.
Most of them are in qml folder. All mods come with comment(flag) - //hado

Some libs needed for compile for Windows were added to this repo (mzfk poppler(thx to @luspi) and others).

What this mode includes?<br>
~~1. darker background for mainmenu, metainfo, thumbnailbar~~ now at official 3.1<br>
2. regular font weight everywhere instead bold<br>
~~3. increased font-size at some places and using default Windows 10/11 font-family Segoe UI~~  now at official 3.2<br>
4. some elements now are more rounded<br>
~~5. image thumbs at file browser now are unmuted (disabled grey overlay)~~ now at official 3.1<br>
~~6. thumbnailbar - added some space for thumbs (diffrent from current spacing from pq settings)~~  now at official 3.2<br>
7. most popular 16:9 resolution now have label at status info - hd, 4k, 8k<br>
~~8. added shortcut for "No fit in window" for always show images at 100% size~~ now at official 3.1<br>
~~9. removed Chromecast from Windows version (cuz doesn't work at present moment)~~ now at official 3.1<br>
10. decreased mainmenu width for EN (default) lang.<br>
~~11. added hover effect at thumbnail bar~~ now at official 3.3<br>
~~12. reverted ContextMenu to old transparent~~<br>
~~13. recolored face tagger~~<br>
14. cleaned settings manager ui<br>

## SCREENSHOTS
<details>
<summary>main window, file browser</summary>
![main window](screenshots/h31_01.jpg)
![image browser](screenshots/h31_02.png)
![settings manager](screenshots/h31_03.png)
</details>

## INSTALL
https://gitlab.com/hadoukez/photo-qt-hadouken-dark-mod/-/releases


## PhotoQt v3.0
__Copyright (C) 2011-2023, Lukas Spies (Lukas@photoqt.org)__
__License:__ GPLv2 (or later)
__Website:__ https://photoqt.org

PhotoQt is a fast and highly configurable image viewer with a simple and nice interface.

PhotoQt is available in the repositories of an increasing number of Linux distributions, and can also be installed in several other ways (Windows installer, Flatpak, etc.). [Check the website](https://photoqt.org/down) to get more information on that, or see below for instructions about how to build PhotoQt from scratch.

***************
## BUILDING ON WINDOWS

PhotoQt offers installers for pre-built binaries on its website: https://photoqt.org/downpopupwindows

If you prefer to build it yourself, this process is not as hard as it might seem at first. The main challenge in building PhotoQt on Windows lies in getting the environment set up and all dependencies installed.

The following are required dependencies:

1. Install Visual Studio 2019 Community Edition (free, be sure to install the 'Desktop Development with C++' workload)
    - Website: https://visualstudio.microsoft.com/
2. Install CMake
    - Website: https://cmake.org/
    - In the installer set the system path option to Add CMake to the system PATH for all users
3. Install Qt 5.15
    - Website: https://qt.io
    - In the installer, make sure to install all required modules as listed above under dependencies
    - After installation, confirm that your installation of Qt finds both CMake and the compiler installed in steps 1 and 2

The following dependencies are recommended but can be disabled through CMake if not wanted:

1. LibArchive: https://libarchive.org/
2. Exiv2: https://exiv2.org/
3. ImageMagick: https://imagemagick.org/
4. LibRaw: https://www.libraw.org/
5. pugixml: https://pugixml.org/
6. Poppler: https://poppler.freedesktop.org/
7. FreeImage: https://freeimage.sourceforge.io/
8. DevIL: http://openil.sourceforge.net/

Make sure that any installed dependency is added to the system path, or otherwise you need to explicitely point CMake to the right location for each of them. Regardless, CMake might have to be explicitely pointed to the library/include paths of some of the dependencies by specifying `target_include_directories()` and `target_link_libraries()`.

Once all the requried and desired dependencies are installed, then the source code of PhotoQt can be fetched from the website (https://photoqt.og/down). One way to build PhotoQt is to load it in the IDE QtCreator that is part of the Qt installation.
